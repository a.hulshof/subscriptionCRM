package nl.base10.bvh.abonnementenCRM.web;

import java.awt.TextField;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import nl.base10.bvh.abonnementenCRM.data.Person;

//@Route("list")
@Route
public class MainView extends VerticalLayout {

	private static final long serialVersionUID = 1L;

	public MainView() {
		add(new Text("List view"));
//		add(new Anchor("/", "Back"));
		add(personGrid());
	}

	public Grid<Person> personGrid() {
		Grid<Person> personGrid = new Grid<>();
		List<Person> items = new ArrayList<>();
		items.add(Person.builder().firstName("Tony").lastName("Hawk").build());
		items.add(Person.builder().firstName("BillyJoe").lastName("Armstrong").build());
		items.add(Person.builder().firstName("Tom").lastName("Morello").build());
		Grid.Column<Person> firstNameColumn = personGrid.addColumn(Person::getFirstName).setHeader("FirstName");
		Grid.Column<Person> lastNameColumn = personGrid.addColumn(Person::getLastName).setHeader("LastName");

		Button addButton = new Button("Add Item", event -> {
			items.add(Person.builder().firstName("X").lastName("Y").build());
			personGrid.getDataProvider().refreshAll();
		});

		Button removeButton = new Button("Remove Item", event -> {

		    items.remove(items.size() - 1);
			personGrid.getDataProvider().refreshAll();
		});
		personGrid.setItems(items);
		FooterRow footerRow = personGrid.appendFooterRow();
		footerRow.getCell(firstNameColumn).setComponent(addButton);
		footerRow.getCell(lastNameColumn).setComponent(removeButton);
		return personGrid;
	}
}