package nl.base10.bvh.abonnementenCRM;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AbonnementenCrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbonnementenCrmApplication.class, args);
	}

}
