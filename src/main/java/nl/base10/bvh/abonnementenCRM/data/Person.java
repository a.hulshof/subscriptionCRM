package nl.base10.bvh.abonnementenCRM.data;

import java.time.LocalDate;
import java.util.List;

import javax.management.relation.RelationType;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Person {

	String firstName;
	String lastName;
	LocalDate birthDay;
	List<Subscription> subscriptions;
	List<RelationType> relationTypes;
}
