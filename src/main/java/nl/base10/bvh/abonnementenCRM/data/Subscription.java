package nl.base10.bvh.abonnementenCRM.data;

import java.time.LocalDate;

import javax.management.relation.RelationType;

import lombok.Data;

@Data
public class Subscription {

	LocalDate startDate;
	LocalDate endDate;
	String description;
	String code;
	String DoW;
	String startTime;
	RelationType relationType;
	boolean invoiced;
	boolean paid;

}
